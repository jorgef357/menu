SHELL:=/bin/bash
BASE_DIR := $(realpath ./)
PREFIX = $(DESTDIR)/usr/local
BINDIR = $(PREFIX)/bin

install:
	go get github.com/kardianos/osext && \
        go get github.com/spf13/viper && \
        go build && \
        rm -f $(BINDIR)/menu > /dev/null 2>&1 && \
        ln -s $(BASE_DIR)/menu.sh $(BINDIR)/menu
