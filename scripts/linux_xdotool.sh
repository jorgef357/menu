#!/bin/bash

USER=$1
IP=$2
KEY=$3
SSHKEY=""

if [ "NA$KEY" != "NA" ]; then
    SSHKEY=" -i ${KEY}"
fi

cmd="ssh -T -o UserKnownHostsFile=/dev/null -o ServerAliveInterval=30 -o StrictHostKeyChecking=no ${USER}@${IP}"
eval "$cmd"
