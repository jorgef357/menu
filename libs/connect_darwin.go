
// +build darwin

package connect

import (
	"os/exec"
	"os"
)

func Username() string {
	return os.Getenv("USER")
}


func SshConn(script string, ip string, username string, sshkey string) {
	
	
	command := script + " " + username + " " + ip + " " + sshkey
    
	_, err := exec.Command("bash", "-c", command).Output()
	if err != nil {
		panic(err)	
	}

}

func CsshConn(script string, list string) {
	
	
	command := script + " " + list

	_, err := exec.Command("bash", "-c", command).Output()
	if err != nil {
		panic(err)
	}

}