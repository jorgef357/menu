#!/bin/bash
DIR=$(dirname $(readlink -f /usr/local/bin/menu))

if [ $# -lt 3 ]; then
  cd ${DIR} && ./menu $@
else
  exec_ssh=$(cd ${DIR} && ./menu $@)
  gnome-terminal --tab --active -e "$exec_ssh"
fi

