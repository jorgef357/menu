#!/bin/bash

USER=$1
IP=$2
KEY=$3
SSHKEY=""
if [ ${KEY:NA} != "NA" ]; then
    SSHKEY=" -i ${KEY}"
fi

cmd="ssh -o UserKnownHostsFile=/dev/null -o ServerAliveInterval=30 -o StrictHostKeyChecking=no ${USER}@${IP} ${SSHKEY}"

osascript -e "tell application \"Terminal\"" \
-e "tell application \"System Events\" to keystroke \"t\" using {command down}" \
-e "do script \"${cmd}\" in front window" \
-e "end tell" 

