#!/bin/bash

USER=$1
IP=$2
KEY=$3
SSHKEY=""
if [ ${KEY:NA} != "NA" ]; then
    SSHKEY=" -i ${KEY}"
fi

cmd="ssh -o UserKnownHostsFile=/dev/null -o ServerAliveInterval=30 -o StrictHostKeyChecking=no ${USER}@${IP} ${SSHKEY}"

osascript -e "tell application \"iTerm2\"" \
-e "tell current window" \
-e "create tab with default profile command  \"${cmd}\"" \
-e "end tell" \
-e "end tell"
